# Welcome!

This project will be used for submitting your homework assignments from №5 to ... .<br>
Here you will find a step-by-step instruction. <br>
However, **PLEASE READ THIS DOCUMENT TO THE END**.

## Instruction

- Visit [GitLab](https://gitlab.com) page and create account
- [Fork current project as private]((https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html))
- [Add me as member](https://docs.gitlab.com/ee/user/project/members/) for project with DEVELOPER role
- Сlone you project 
     ```bash
      git clone https://github.com/your-username/your-forked-repo.git
     ```
**My account in Gitlab - @kovalchuk.dima29
  
## Example for send HW
Let's imagine that we are submitting HW 5 task 2

1. Pull all updates while on the main branch:
   ```bash
      git pull origin main
   ```
2. Create new branch:
   ```bash
   git checckout -b hw5
   ```
3. Open class src/main/hws/hw5/hw5_2/App.class and in method isPerfectSquare performing the task  <br>
![img.png](src/main/resources/images/hwexample.png)

and check that all cases have PASS result
<a id="passResult"></a>
![passResult.png](src/main/resources/images/passResult.png)

7. Add files to index by UI or git command:
   ```bash
   git add  /path/file
   ```
   or for add all changed files:
   ```bash
   git add  .
   ```
8. Save changes by UI or git command:
   ```bash
   git commit -m  "The message should describe the changes you have made"
   ```
9. Send your local commits to the remote repository
   ```bash
   git push --set-upstream origin hw6
   ```
10. Open your Gitlab account and create Merge Request
11. In description, you can add more details about changes
12. Set me as reviewer
13. Sent MR and screen as in [p3](#passResult) to Hillel 
14. After receiving the grade, merge the MR
