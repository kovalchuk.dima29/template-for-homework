package hws.engine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class DataReader {

    public static <T> List<T> readCSV(String methodName, Function<String[], T> mapper) {
        String csvFile = "src/main/resources/data/" + methodName +".csv";
        String line;
        String cvsSplitBy = ",";
        List<T> result = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] values = line.split(cvsSplitBy);
                result.add(mapper.apply(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
