package hws.engine;

import static hws.engine.Colors.*;

public class Results {

    public static String printPassResult(Object params1, String method,  Object result){
        return GREEN + "PASS" + RESET + ": " + method + "("+ params1.toString()+ ") -> " + result.toString();
    }


    public static String printFailResult(Object params1, String method, Object expected, Object result){
        String ret = "**********************" + "\n";
        ret += RED + "FAIL"  + RESET + ": " + method +"("+ params1.toString()+ ") -> " + result.toString()
                + "      Expected: "+ YELLOW + expected.toString() + RESET;
        ret += "\n" + "**********************";
        return ret;
    }
}
