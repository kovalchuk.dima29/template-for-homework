package hws.hw7;


public class App {

    /**
     * Create a method to calculate the average value of the elements in an array of numbers.
     * Implement functionality to find the maximum and minimum values in an array.
     * Write a method to print the elements of an array in reverse order.
     * Create a program that checks if a given number is present in an array provided by the user.
     */

    static double averageValue(int[] array)
    {
        //your code
        return 1.0;
    }

    static int maxValue(int[] array)
    {
        //your code
        return 1;

    }

    static int minValue(int[] array)
    {
        //your code
        return 1;

    }

    static int[] reverseArray(int[] array)
    {
        //your code
        return new int[1];

    }

    static boolean isPresentNumber(int[] array, int element)
    {
        //your code
        return false;
    }

    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]) {
        TestingUtils.runTests();
    }
}
