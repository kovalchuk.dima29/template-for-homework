package hws.hw7;


import java.util.*;

import static hws.engine.Colors.*;
import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw7.App.*;

class TestingUtils {

    static void runTests() {
        String methodName = "array";
        int i = 1;
        List<List<String>> data = readCSV(methodName, values -> {
            String param1 = values[0];
            String param2 = values[1];
            String param3 = values[2];
            String param4 = values[3];
            String param5 = values[4];
            String param6 = values[5];
            String param7 = values[6];

            return new ArrayList<>(Arrays.asList(param1, param2, param3, param4, param5, param6, param7));
        });
        for (List<String> result : data) {

            String param1 = result.get(0);
            double average = averageValue(convertToArray(result.get(0)));
            int maxValue = maxValue(convertToArray(result.get(0)));
            int minValue = minValue(convertToArray(result.get(0)));
            int[] reverseArray = reverseArray(convertToArray(result.get(0)));
            boolean isPresentNumber = isPresentNumber(convertToArray(result.get(0)), Integer.parseInt(result.get(6)));

            double expectedAverage = Double.parseDouble(result.get(1));
            int expectedMaxValue = Integer.parseInt(result.get(2));
            int expectedMinValue = Integer.parseInt(result.get(3));
            int[] expectedReverseArray = convertToArray(result.get(4));
            boolean expectedShow = Boolean.parseBoolean(result.get(5));

            System.out.println(PURPLE + "HW 7. Case #" + i);
            printResult(param1, average, expectedAverage, "Average");
            printResult(param1, maxValue, expectedMaxValue, "MaxValue");
            printResult(param1, minValue, expectedMinValue, "MinValue");
            printResultForArray(param1, reverseArray, expectedReverseArray, "ReverseArray");
            printResult(result.get(6) + " in " + param1, isPresentNumber, expectedShow, "IsPresentNumber");
            i++;


        }
    }

    private static int[] convertToArray(String str)
    {

        String[] stringArray = str.split(" ");

        // Створюємо масив цілих чисел такого ж розміру, як і масив рядків
        int[] intArray = new int[stringArray.length];

        // Перетворюємо кожен елемент масиву рядків у ціле число
        for (int i = 0; i < stringArray.length; i++) {
            intArray[i] = Integer.parseInt(stringArray[i]);
        }

        return intArray;
    }

    private static void printResult(String params, Object actual, Object expected, String methodName )
    {
        if (actual.equals(expected)) {
            System.out.println(printPassResult(params, methodName, expected));
        } else {
            System.out.println(printFailResult(params, methodName, expected, actual));
        }
    }

    private static void printResultForArray(String params, int[] actual, int[] expected, String methodName)
    {
        if (Arrays.equals(actual, expected)) {
            System.out.println(printPassResult(Arrays.toString(expected), methodName, Arrays.toString(actual)));
        } else {
            System.out.println(printFailResult(params, methodName, Arrays.toString(expected), Arrays.toString(actual)));
        }
    }

}

