package hws.hw6.hw6_5;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw6.hw6_5.App.xyzThere;

class TestingUtils {
    static void runTests() {
        String methodName = "xyzThere";
        List<Map.Entry<String, Boolean>> data = readCSV(methodName, values -> {
            String param = values[0];
            Boolean expected = Boolean.parseBoolean(values[1]);
            return new AbstractMap.SimpleEntry<>(param, expected);
        });

        for (Map.Entry<String, Boolean> entry : data) {
            boolean result = xyzThere(entry.getKey());
            if (result == entry.getValue()) {
                System.out.println(printPassResult(entry.getKey(), methodName, result));
            } else {
                System.out.println(printFailResult(entry.getKey(), methodName, entry.getValue(), result));
            }
        }
    }
}
