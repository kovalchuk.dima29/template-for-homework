package hws.hw6.hw6_5;

class App {

    /**
     * Return true if the given string contains an appearance of "xyz" where the xyz is not directly preceeded by a period (.).
     * So "xxyz" counts but "x.xyz" does not.
     * xyzThere("abcxyz") → true
     * xyzThere("abc.xyz") → false
     * xyzThere("xyz.abc") → true
     */

    static boolean xyzThere(String str) {

        //your code
        return false;
    }


    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]) {
        TestingUtils.runTests();
    }
}
