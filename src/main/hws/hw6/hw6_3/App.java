package hws.hw6.hw6_3;


class App {

    /**
     * Given a string, does "xyz" appear in the middle of the string?
     * To define middle, we'll say that the number of chars to the left and right of the "xyz" must differ by at most one.
     * This problem is harder than it looks.
     * EXPECTATIONS:
     * xyzMiddle("AAxyzBB") → true
     * xyzMiddle("AxyzBB") → true
     * xyzMiddle("AxyzBBB") → false
     */

    static boolean xyzMiddle(String word) {

        //your code
        return true;

    }


    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]) {
        TestingUtils.runTests();
    }
}

