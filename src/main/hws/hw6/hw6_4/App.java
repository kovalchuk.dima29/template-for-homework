package hws.hw6.hw6_4;


class App {

    /**
     * Look for patterns like "zip" and "zap" in the string -- length-3, starting with 'z' and ending with 'p'.
     * Return a string where for all such words, the middle letter is gone, so "zipXzap" yields "zpXzp"
     * EXPECTATIONS:
     * zipZap("zipXzap") → "zpXzp"
     * zipZap("zopzop") → "zpzp"
     * zipZap("zzzopzop") → "zzzpzp"
     */

    static String zipZap(String word) {

        //your code
        return "";
    }


    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
  public   static void main(String args[]) {
        TestingUtils.runTests();
    }
}
