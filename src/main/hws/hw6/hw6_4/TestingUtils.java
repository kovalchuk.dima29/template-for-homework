package hws.hw6.hw6_4;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw6.hw6_4.App.zipZap;

class TestingUtils {

    static void runTests() {
        String methodName = "zipZap";
        List<Map.Entry<String, String>> data = readCSV(methodName, values -> {
            String param = values[0];
            String expected = values[1];
            return new AbstractMap.SimpleEntry<>(param, expected);
        });

        for (Map.Entry<String, String> entry : data) {
            String result = zipZap(entry.getKey());
            if (result.equals(entry.getValue())) {
                System.out.println(printPassResult(entry.getKey(), methodName, result));
            } else {
                System.out.println(printFailResult(entry.getKey(), methodName, entry.getValue(), result));
            }
        }
    }
}
