package hws.hw6.hw6_2;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw6.hw6_2.App.mixString;

class TestingUtils {

    static void runTests() {
        String methodName = "mixString";
        List<Map.Entry<Map.Entry<String, String>, String>> data = readCSV(methodName, values -> {
            String param = String.valueOf(values[0]);
            String param2 = String.valueOf(values[1]);
            String expected = String.valueOf(values[2]);
            return new AbstractMap.SimpleEntry<>(new AbstractMap.SimpleEntry<>(param, param2), expected);
        });

        for (Map.Entry<Map.Entry<String, String>, String> entry : data) {
            String result = mixString(entry.getKey().getKey(), entry.getKey().getValue());
            if (result.equals(entry.getValue())) {
                System.out.println(printPassResult(entry.getKey(), methodName, result));
            } else {
                System.out.println(printFailResult(entry.getKey(), methodName, entry.getValue(), result));
            }
        }
    }
}


