package hws.hw6.hw6_2;


class App {

    /**
     *
     * Given two strings, a and b, create a bigger string made of the first char of a, the first char of b, the second char of a, the second char of b, and so on.
     * Any leftover chars go at the end of the result.
     * EXPECTATIONS:
     * mixString("abc", "xyz") → "axbycz"
     * mixString("Hi", "There") → "HTihere"
     * mixString("xxxx", "There") → "xTxhxexre"
     */

    static String mixString(String a, String b) {

        //your code
        return "";
    }


    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]) {
        TestingUtils.runTests();
    }
}

