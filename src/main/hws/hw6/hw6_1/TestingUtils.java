package hws.hw6.hw6_1;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw6.hw6_1.App.repeatEnd;

class TestingUtils {

    static void runTests() {
        String methodName = "repeatEnd";
        List<Map.Entry<Map.Entry<String, Integer>, String>> data = readCSV(methodName, values -> {
            String param = String.valueOf(values[0]);
            Integer param2 = Integer.parseInt(values[1]);
            String expected = String.valueOf(values[2]);
            return new AbstractMap.SimpleEntry<>(new AbstractMap.SimpleEntry<>(param, param2), expected);
        });

        for (Map.Entry<Map.Entry<String, Integer>, String> entry : data) {
            String result = repeatEnd(entry.getKey().getKey(), entry.getKey().getValue());
            if (result.equals(entry.getValue())) {
                System.out.println(printPassResult(entry.getKey(), methodName, result));
            } else {
                System.out.println(printFailResult(entry.getKey(), methodName, entry.getValue(), result));
            }
        }
    }
}


