package hws.hw5.hw5_4;

class App {

    /**
    Write a program that accepts a numerical value from 1 to 5 and outputs the corresponding student's grade.

     EXPECTATIONS:
    getGrade(5) → "Excellent"
    getGrade(4) → "Good"
    getGrade(3) → "Bad"
    getGrade(2) → "Average"
    getGrade(1) → "Poor"
    getGrade(0) → "Unknown grade"
    getGrade(-3) → "Unknown grade"
     */

    static String getGrade(int grade) {
        //your code
        return "";
    }



    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]){
        TestingUtils.runTests();
    }
}
