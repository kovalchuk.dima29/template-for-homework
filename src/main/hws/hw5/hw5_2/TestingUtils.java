package hws.hw5.hw5_2;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw5.hw5_2.App.isPerfectSquare;

class TestingUtils {

    static void runTests() {
        String methodName = "isPerfectSquare";

        List<Map.Entry<Integer, Boolean>> data = readCSV(methodName, values -> {
            Integer param = Integer.parseInt(values[0]);
            Boolean expected = Boolean.parseBoolean(values[1]);
            return new AbstractMap.SimpleEntry<>(param, expected);
        });

        for (Map.Entry<Integer, Boolean> entry : data) {
            boolean result = isPerfectSquare(entry.getKey());
            if (result == entry.getValue()) {
                System.out.println(printPassResult(entry.getKey(), methodName, result));
            } else {
                System.out.println(printFailResult(entry.getKey(), methodName, entry.getValue(), result));
            }
        }
    }
}
