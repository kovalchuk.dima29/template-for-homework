package hws.hw5.hw5_2;

class App {

    /**
     Write a program in Java that checks whether a number is a perfect square.

     EXPECTATIONS:
     isPerfectSquare(9) -> true
     isPerfectSquare(11)  -> false
     isPerfectSquare(16) -> true
     */

    static boolean isPerfectSquare(int number) {

        //your code
        return  false;
    }


    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]){
        TestingUtils.runTests();
    }
}
