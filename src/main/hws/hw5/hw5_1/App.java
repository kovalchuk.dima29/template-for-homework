package hws.hw5.hw5_1;

class App {

	/**
	 Write a program that outputs a message indicating whether the user is an adult (age 18 and over) or not.

	 EXPECTATIONS:
	 checkAge(19) -> true
	 checkAge(1)  -> false
	 checkAge(-1) -> false
	 */

	static boolean checkAge(int age) {

		/// your code
		return false;
	}


//----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
	public void main(String args[]){
		TestingUtils.runTests();
	}
}

