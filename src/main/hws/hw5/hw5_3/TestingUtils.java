package hws.hw5.hw5_3;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static hws.engine.DataReader.readCSV;
import static hws.engine.Results.printFailResult;
import static hws.engine.Results.printPassResult;
import static hws.hw5.hw5_3.App.calculateSumUpToN;

public class TestingUtils {
    static void runTests() {
        String methodName = "calculateSumUpToN";

        List<Map.Entry<Integer, String>> data = readCSV(methodName, values -> {
            Integer param = Integer.parseInt(values[0]);
            String expected = values[1];
            return new AbstractMap.SimpleEntry<>(param, expected);
        });

        for (Map.Entry<Integer, String> entry : data) {
            String result = calculateSumUpToN(entry.getKey());
            if (result.equals(entry.getValue())) {
                System.out.println(printPassResult(entry.getKey(), methodName, result));
            } else {
                System.out.println(printFailResult(entry.getKey(), methodName, entry.getValue(), result));
            }
        }
    }
}
