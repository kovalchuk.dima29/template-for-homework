package hws.hw5.hw5_5;


class App {

    /**
     Returns true if the number is prime, false otherwise.
     Primes are integers greater than one with no positive divisors besides one and itself.
     You will find modulus (%) and Math.sqrt() useful

     EXPECTATIONS:
     isPrime(1) → false
     isPrime(2) → true
     isPrime(3) → true
     */

    static boolean isPrime(int num) {
        //your code
        return true;
    }



    //----------------------STARTING POINT OF PROGRAM. IGNORE BELOW --------------------//
    public static void main(String args[]){
        TestingUtils.runTests();
    }
}
